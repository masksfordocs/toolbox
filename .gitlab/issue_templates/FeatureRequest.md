
# Feature Request Template 

<!---
Please feel free to add/remove sections as your consider necessary
when filing new feature requests.

If you add new items, please also consider contributing to this template
by raising a MR in the .gitlab folder of this repo.

Thanks!
-->

## Type 

Describe if this a one-off feature or a series of features

## Who 

Who is requesting this: identify themselves and their team so that you can easily follow up with them

## Current situation

Describe the problem that the user has: add the problem that the user has. This is key for you to differentiate between people that have deeply thought about what problem this will solve for users versus those that are just in love with their idea

## What this change is about

Describe the nature of the change requested

## Priority

Describe how urgent this is and why: give the requester the space to give insight into the urgency of this issue and why

## Impact

Describe the impact that this has on the user: articulate how solving this problem will make your user’s life better. You can add impact categories that matter to you to ensure consistency

Describe the reach of this problem: requester will describe how many users will be positively impacted or leverage this feature.

## Cost of not doing

Describe the problems that would occur if this need was not addressed. To ensure consistency add categories that matter to your business such as “increased support costs” or “risk of losing customers”

## Business Value

- Primary Goal
- Secondary Goal

## Evidence

Describe the evidence that you have on the need for this request: validation of user problem and desired outcome

## How we may solve this

Describe if you have any ideas on how we may solve this: give the requester the space to help and suggest ideas

## Related items

If necessary, please paste link to other issues or links that might help
to better understand the scope of this feature request
