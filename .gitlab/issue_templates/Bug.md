# Issue Template

<!--
The sections below provide with guidelines and suggestions that might
help capture information when submitting a new ticket.

They are optional, so please feel free to fill them according to your
need, or even remove those that might not apply.

If you need to add new sections, please consider contributing to this
template in the .gitlab folder of this repo.
-->

## Context

Please provide any relevant information about your setup. This is important in case the issue is not reproducible except for under certain conditions.

* Operating System: eg. Windows, Linux, macOS, iOS, Android, etc.. 
* Environment: eg. Dev, Staging, Production
* Browser: Chrome, Firefox, Safari
* Toolchain version: Eg. Erlang/OTP 22.0, Elixir 1.10.1, npm 6.14.4

## Expected Behavior

Please describe the behavior you are expecting

## Current Behavior

What is the current behavior?

## Failure Information (for bugs)

Please help provide information about the failure if this is a bug. If it is not a bug, please remove the rest of this template.

### Steps to Reproduce

Please provide detailed steps for reproducing the issue.

1. step 1
2. step 2
3. you get it...

### Failure Logs

Please include any relevant log snippets or files here.

### Screenshots  

Please include any relevant screenshots here.
