# Merge Request Template

<!--
The sections below provide with guidelines and suggestions that might
help capture information when submitting a MR request, so that it makes
it easier for others to review it.

They are optional, so please feel free to fill them according to your
need, or even remove those that might not apply.

If you need to add new sections, please consider contributing to this
template in the .gitlab folder of this repo.
-->

## Description

Please include a summary of the change and which issue is fixed. Please also include relevant motivation and context. List any dependencies that are required for this change.

Fixes # (issue)

## How this change makes you feel

![](https://media.giphy.com/media/cFkiFMDg3iFoI/giphy.gif)

## Type of change 

- [ ] Non user facing
- [ ] User facing - breaking change)
- [ ] User impacting - non breaking change
- [ ] Emergency

## Affected components

- [ ] Module A
- [ ] Module B
- [ ] Module C

## How Has This Been Tested?

Please describe the tests that you ran to verify your changes. Please also note any relevant details for your test configuration.

- [ ] Test A
- [ ] Test B

## How should this be deployed

- [ ] Will not cause downtime
- [ ] Will cause downtime

## Documentation updates

- [ ] No changes required
- [ ] I have made corresponding changes to the documentation

## Communication with other teams

- [ ] Backend
- [ ] Frontend
- [ ] Devops

## Other checks:

<!--
Please feel free to comment/uncomment/add items as you see fit
-->

- [ ] Elixir modules and public functions are documented
- [ ] Elixir public functions have typespecs defined
- [ ] Any dependent changes have been merged and published in downstream modules
